```@meta
CurrentModule = Ueauty
```

# Ueauty

The Julia language is unique in that the base language provides unicode aliases for a
number of commonly used symbols, the default language REPL has built-in [LaTeX to
unicode](https://docs.julialang.org/en/v1.7-dev/manual/unicode-input/) conversion, and it
is arguably the only programming language where use of unicode symbols is common even in
purely English language programs.  Additionally, nearly all Julia editor plug-ins contain
some form of LaTeX to unicode conversion.

Unfortunately, while the provided aliases are extremely useful, there are quite a few
missed opportunities as well.

This package is meant to provide additional aliases for universally recognized symbols as
simply as possible, with 0 dependencies.

!!! note

    While many monospace fonts have good unicode support for all Greek letters as well as
    some of the more common mathematical symbols, the
    [JuliaMono](https://github.com/cormullion/juliamono) font has been designed
    specifically with readable, consistent unicode in mind.  In particular alternate type
    faces such as `\mathcal` (`𝒜, ℬ, 𝓍, 𝓎`), `\mathfrak` (`𝔄, 𝔅, 𝔵, 𝔶`), `\mathbb` (`ℝ, ℤ,
    ℂ`), annotations `e⃗, ŷ, x̄`, as well as some operations such as `→, ⇒` are *much* more
    consistent, readable and useable than in most other monospace fonts.  I recommend you
    at least try it out.

## Zero Dependencies
This package comes with a guarantee that no version of it will have any external
dependencies except possibly in the Julia stdlib.  This is to ameliorate any possible
concerns about package bloat, and to make its use as close to the base language as
possible

## Safe, Universally Recognized Definitions
This package only defines symbols which are universally recognized (of course, your
mileage may vary) and unlikely to be used in existing code (at least with any meaning
other than used here).

For example, this package does *NOT* define `const ϵ = eps` (likewise it does *NOT* define
`ε`) both because those are expected to be commonly used as variable names in code, and
because they are not universally recognized as floating point ``\epsilon``.

## Macros
Unicode aliases are provided by the following macros, all of which are exported from the
package.

Aliases can be declared by category, or *all* available can be defined with
```@docs
Ueauty.@ueauty
```

### Types
```@docs
Ueauty.@ueauty_types
```

This macro provides the following symbols.

| Symbol | LaTeX-to-Unicode | Defined as | Description |
| ------ | :--              | :--        | :--         |
| `ℝ`    | `\bbR`           | `Real`     | Real number abstract type. |
| `ℂ`    | `\bbC`           | `Complex`  | Complex number abstract type. |
| `ℤ`    | `\bbZ`           | `Integer`  | Integral number abstract type. |
| `ℚ`    | `\bbQ`           | `Rational` | Rational number abstract type. |

### Constants
```@docs
Ueauty.@ueauty_constants
```

This macro provides the following symbols.

| Symbol | LaTeX-to-Unicode | Defined as   | Description |
| ------ | :--              | :--          | :--         |
| `𝒾`    | `\scri`          | `im`         | The imaginary unit. |
| `∞`    | `\infty`         | `Inf`        | Floating point infinity. |
| `∅`    | `\emptyset`      | `Set{T}()`   | An empty set of type `T`, for example `∅(Float64) == Set(Float64[])`. |

```@docs
Ueauty.∅
```

### Operators
```@docs
Ueauty.@ueauty_operators
```

This macro provides the following symbols.

| Symbol | LaTeX-to-Unicode | Defined as   | Description |
| ------ | :--              | :--          | :--         |
| `∑`    | `\sum`           | `sum`        | Summation operation. |
| `∏`    | `\prod`          | `prod`       | Product operation. |
| `⇒`    | `\Rightarrow`    | `=>`         | The `Pair` operator. |
| `▷`    | `\triangleright` | `\|>`         | The "pipe" operator. |


!!! note

    The `∑` and `∏` characters are *NOT* the same as `Σ` and `Π`, so the symbols `Σ` and
    `Π` are still available for regular use.  With a good enough unicode font the
    distinction should be clear.
