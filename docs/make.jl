using Ueauty
using Documenter

DocMeta.setdocmeta!(Ueauty, :DocTestSetup, :(using Ueauty); recursive=true)

makedocs(;
    modules=[Ueauty],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Ueauty.jl/blob/{commit}{path}#{line}",
    sitename="Ueauty.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Ueauty.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
