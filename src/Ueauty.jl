module Ueauty

"""
    @ueauty_types

Declare nice unicode aliases for Julia types.  See documentation for an
explicit list.
"""
macro ueauty_types()
    esc(quote
        const ℝ = Real
        const ℂ = Complex
        const ℤ = Integer
        const ℚ = Rational
        nothing
    end)
end

"""
    ∅(T=Any)

Return an empty set with elements of type `T`.
"""
∅(::Type{T}=Any) where {T} = Set{T}()

"""
    @ueauty_constants

Declare nice unicode aliases for some special constant values.  See documentation for an
explicit list.
"""
macro ueauty_constants()
    esc(quote
        const 𝒾 = im
        const ∞ = Inf
        using Ueauty: ∅
        nothing
    end)
end

"""
    @ueauty_operators

Declare nice unicode aliases for some common operators.  See documentation for an explicit
list.
"""
macro ueauty_operators()
    esc(quote
        const ∑ = sum
        const ∏ = prod
        const ⇒ = =>
        const ▷ = |>
        nothing
    end)
end

"""
    @ueauty

Declare all unicode aliases defined in Ueauty.  See documentation for an explicit list.
"""
macro ueauty()
    esc(quote
        @ueauty_types
        @ueauty_constants
        @ueauty_operators
    end)
end


export @ueauty_types, @ueauty_constants, @ueauty_operators, @ueauty

end
