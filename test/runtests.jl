using Ueauty
using Test

@ueauty

@testset "Ueauty.jl" begin
    @test ℝ ≡ Real
    @test ℂ ≡ Complex
    @test ℤ ≡ Integer
    @test ℚ ≡ Rational

    @test 𝒾 ≡ im
    @test ∞ ≡ Inf
    @test ∅(Int) == Set(Int[])

    @test ∑ ≡ sum
    @test ∏ ≡ prod
    @test (⇒) ≡ (=>)
    @test (▷) ≡ (|>)
end
